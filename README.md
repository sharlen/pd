# 明道云私有部署版

[![ming](https://svg.hamm.cn/badge.svg?key=I%20%E2%9D%A4%20MY%20TEAM&value=%E6%98%8E&color=ff4500)](https://www.mingdao.com) [![star](https://svg.hamm.cn/gitee.svg?user=mingdaocloud&project=private-deployment&type=star)](https://gitee.com/mingdaocloud/private-deployment/stargazers)
[![star](https://svg.hamm.cn/gitee.svg?user=mingdaocloud&project=private-deployment&type=commit)](https://gitee.com/mingdaocloud/private-deployment/commits/master)

<img src="https://user-images.githubusercontent.com/7261408/82203093-67ae1600-9935-11ea-8cd9-89b61b47b38f.png" alt="logo" height="150px"/>

[明道云](https://www.mingdao.com) 是一个企业软件的快速设计和开发工具。但不需要任何代码编写，普通业务人员就能掌握使用。通过灵活的功能组件，让企业可以搭建个性化的 CRM、ERP、OA、项目管理、进销存等系统，你可以用它管理生产、销售、采购、人事等所有企业活动。明道云私有部署基于镜像模式，旨在提供给用户一个能快速安装与体验的 APaaS 平台。

## 快速体验

- [单机模式快速安装](https://docs.pd.mingdao.com/deployment/docker-compose/standalone/quickstart.html)

- [集群模式部署指南](https://docs.pd.mingdao.com/deployment/docker-compose/cluster/guide.html)

- [Kubernetes 部署指南](https://docs.pd.mingdao.com/deployment/kubernetes/)

## 产品简介

![](https://images.gitee.com/uploads/images/2020/1128/183512_cd178543_7544271.png "1.png")
<br/><br/>
![](https://images.gitee.com/uploads/images/2020/1128/183522_5b254cbb_7544271.png "3.png")
<br/><br/>
![](https://images.gitee.com/uploads/images/2020/1128/183530_ca753456_7544271.png "4.png")
<br/><br/>
![](https://images.gitee.com/uploads/images/2020/1128/183537_7af8c4e7_7544271.png "5.png")
<br/><br/>
![](https://images.gitee.com/uploads/images/2020/1128/183544_8f8196b7_7544271.png "6.png")
<br/><br/>
![](https://images.gitee.com/uploads/images/2020/1128/183551_628976b5_7544271.png "7.png")
<br/><br/>
![](https://images.gitee.com/uploads/images/2020/1128/183558_68a26720_7544271.png "8.png")
<br/><br/> 
![](https://images.gitee.com/uploads/images/2020/1128/183605_aab8284a_7544271.png "9.png")
<br/><br/>
![](https://images.gitee.com/uploads/images/2020/1128/183612_7c07a03e_7544271.png "10.png") 